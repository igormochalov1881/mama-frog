using System.Collections;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    [SerializeField] private GameObject _menu;
    [SerializeField] private TextMeshProUGUI _caviarScore;
    [SerializeField] private TextMeshProUGUI _timerText;
    [SerializeField] private Image _arrowToTadpoles;
    private SpriteRenderer _sprite;
    private Rigidbody2D _rigidbody2D;
    private StarvingBar _starvingBar;

    private Vector2 _coursorPosition;
    private Vector2 _distanceToCoursor;

    private float _timeBetweenJumps = 0.6f;
    private float _gameTimer = 180;
    private float _sleepTimer = 10;
    private float _jumpForce = 14f;
    private float _jumpTimer;
    private int _maxEatedWarms = 20;
    private int _currentEatedWarmsAmount = 12;
    private int _maxCaviarAmount = 5;
    private int _currentCaviarAmount;
    private bool _isSleeping;
    private bool _isDead;

    private void Start()
    {
        _menu.SetActive(false);
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _sprite = GetComponent<SpriteRenderer>();
        _starvingBar = GetComponent<StarvingBar>();
        _starvingBar.SetMaximumStarving(_maxEatedWarms);
        _starvingBar.SetStarving(_currentEatedWarmsAmount);
        _caviarScore.SetText($"{_currentCaviarAmount} / 5");
    }

    private void Update()
    {
        _coursorPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        _distanceToCoursor = new Vector2(_coursorPosition.x - transform.position.x, _coursorPosition.y - transform.position.y);

        if (_isDead == false)
        {
            _gameTimer -= Time.deltaTime;
            _timerText.SetText($"{Mathf.Round(_gameTimer)}");

            if (_gameTimer <= 0)
            {
                Death();
            }

            if (_currentCaviarAmount >= _maxCaviarAmount)
            {
                Win();
            }

            if (_currentEatedWarmsAmount <= 0)
            {
                StartCoroutine("Sleep");
            }
            else if (_isSleeping == false)
            {
                Flip();

                _jumpTimer += Time.deltaTime;
                if (Input.GetKey(KeyCode.Space) && _jumpTimer > _timeBetweenJumps)
                {
                    Jump();
                }
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Warm" && _isSleeping == false &&
            Mathf.Abs(transform.position.y - collision.transform.position.y) < 0.25f &&
            Mathf.Abs(transform.position.x - collision.transform.position.x) < 0.25f)
        {
            TryEat(collision);
        }
        else if (collision.tag == "Caviar")
        {
            Destroy(collision.gameObject);
            _currentCaviarAmount++;
            _caviarScore.SetText($"{_currentCaviarAmount} / 5");
        }
        else if (collision.tag == "Water")
        {
            Death();
        }
    }

    private void TryEat(Collider2D collision)
    {
        if (_currentEatedWarmsAmount < _maxEatedWarms)
        {
            Destroy(collision.gameObject);
            Eat();
        }
    }

    private void Eat()
    {
        _currentEatedWarmsAmount += 2;
        _starvingBar.SetStarving(_currentEatedWarmsAmount);
    }

    private void Jump()
    {
        _animator.SetTrigger("Jump");
        _rigidbody2D.AddForce(CoursorDirection(_distanceToCoursor.x, _distanceToCoursor.y), ForceMode2D.Impulse);

        --_currentEatedWarmsAmount;
        _starvingBar.SetStarving(_currentEatedWarmsAmount);

        _jumpTimer = 0;
    }

    private Vector2 CoursorDirection(float X, float Y)
    {
        float xDirection, yDirection;

        if (Mathf.Abs(X) > Mathf.Abs(Y))
        {
            xDirection = (_distanceToCoursor.x / Mathf.Abs(_distanceToCoursor.x)) * _jumpForce;
        }
        else
        {
            xDirection = Mathf.Abs(_distanceToCoursor.x) / Mathf.Abs(_distanceToCoursor.y) *
                ((_distanceToCoursor.x / Mathf.Abs(_distanceToCoursor.x)) * _jumpForce);
        }

        if (Mathf.Abs(Y) > Mathf.Abs(X))
        {
            yDirection = (_distanceToCoursor.y / Mathf.Abs(_distanceToCoursor.y)) * _jumpForce;
        }
        else
        {
            yDirection = Mathf.Abs(_distanceToCoursor.y) / Mathf.Abs(_distanceToCoursor.x) *
                ((_distanceToCoursor.y / Mathf.Abs(_distanceToCoursor.y)) * _jumpForce);
        }

        return new Vector2(xDirection, yDirection);
    }

    private IEnumerator Sleep()
    {
        if (_isSleeping == false)
            _currentEatedWarmsAmount += 8;

        _isSleeping = true;
        _animator.SetBool("Sleep", true);
        yield return new WaitForSeconds(_sleepTimer);
        _isSleeping = false;
        _animator.SetBool("Sleep", false);

        _starvingBar.SetStarving(_currentEatedWarmsAmount);
    }

    private void Flip()
    {
        _sprite.flipX = _distanceToCoursor.x > 0 ? false : true;
    }

    private void Win()
    {
        SceneManager.LoadScene(2);
    }

    private void Death()
    {
        _isDead = true;
        _gameTimer = 0;
        _menu.SetActive(true);
        Destroy(transform.gameObject);
    }
}