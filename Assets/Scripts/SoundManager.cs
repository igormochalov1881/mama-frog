﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;

public class SoundManager : MonoBehaviour
{
    private AudioSource _source;
    public AudioClip buttonSound;
    public AudioClip eatWarmSound;
    public AudioClip pickUpTadpoleSound;
    
    public AudioClip backMusic;
    private float _backMusicTime = 171;

    private void OnEnable()
    {
        MapEventsManager.startGame.AddListener(BackSoundController);
        MapEventsManager.buttonPressed.AddListener(ButtonSound);
        MapEventsManager.eatWorm.AddListener(EatingSond);
        MapEventsManager.tadpolePickUp.AddListener(PickUpTadpoleSond);
    }

    public void BackSoundController()
    {
        StartCoroutine("BackSound");
    }
    
    public IEnumerator BackSound()
    {
        while (true)
        {
            _source.PlayOneShot(backMusic);

            yield return new WaitForSeconds(_backMusicTime);
        }
    }

    public void ButtonSound()
    {
        _source.PlayOneShot(buttonSound);
    }
    
    public void EatingSond()
    {
        _source.PlayOneShot(eatWarmSound);
    }
    
    public void PickUpTadpoleSond()
    {
        _source.PlayOneShot(pickUpTadpoleSound);
    }
}