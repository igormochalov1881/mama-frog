using UnityEngine.Events;

public static class MapEventsManager
{
    public static UnityEvent tadpolePickUp = new UnityEvent();
    public static UnityEvent eatWorm = new UnityEvent();
    public static UnityEvent startGame = new UnityEvent();
    public static UnityEvent buttonPressed = new UnityEvent();
}
