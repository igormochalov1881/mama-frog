using UnityEngine;

public class ArrowToTadpoles : MonoBehaviour
{
    [SerializeField] private Transform[] _tadpoles;
    private Vector2[] _tadpolesPositions;
    private Vector2 _nearestTadpolePosition;
    private Vector2 _nearestTadpoleDirection;

    private void Start()
    {
        _tadpolesPositions = new Vector2[_tadpoles.Length];
    }

    private void Update()
    {
        FillAllTadpolesPositions();

        FindNearestTadpole();

        _nearestTadpoleDirection = TargetDirection(transform.position, _nearestTadpolePosition);

        RotateToNearestTadpole(_nearestTadpoleDirection);
    }

    private void RotateToNearestTadpole(Vector2 directionToTarget)
    {
        float _angle = Mathf.Atan2(directionToTarget.y, directionToTarget.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(_angle, Vector3.forward);
    }

    private void FillAllTadpolesPositions()
    {
        for (int i = 0; i < _tadpoles.Length; i++)
        {
            if (_tadpoles[i] != null)
            {
                _tadpolesPositions[i] = _tadpoles[i].position;
            }
        }
    }

    private void FindNearestTadpole()
    {
        _nearestTadpolePosition = _tadpolesPositions[0];

        for (int i = 0; i < _tadpolesPositions.Length; i++)
        {
            if (_tadpolesPositions[i] != null)
            {
                if (Vector2.Distance(transform.position, _nearestTadpolePosition) > Vector2.Distance(transform.position, _tadpolesPositions[i]))
                {
                    _nearestTadpolePosition = _tadpolesPositions[i];
                }
            }
        }
    }

    private Vector2 TargetDirection(Vector2 objectPosition, Vector2 targetPosition)
    {
        float xDirection, yDirection;

        Vector2 distanceToTarget = new Vector2(targetPosition.x - objectPosition.x, targetPosition.y - objectPosition.y);

        if (Mathf.Abs(distanceToTarget.x) > Mathf.Abs(distanceToTarget.y))
        {
            xDirection = distanceToTarget.x / Mathf.Abs(distanceToTarget.x);
        }
        else
        {
            xDirection = Mathf.Abs(distanceToTarget.x) / Mathf.Abs(distanceToTarget.y) * (distanceToTarget.x / Mathf.Abs(distanceToTarget.x));
        }

        if (Mathf.Abs(distanceToTarget.y) > Mathf.Abs(distanceToTarget.x))
        {
            yDirection = distanceToTarget.y / Mathf.Abs(distanceToTarget.y);
        }
        else
        {
            yDirection = Mathf.Abs(distanceToTarget.y) / Mathf.Abs(distanceToTarget.x) * (distanceToTarget.y / Mathf.Abs(distanceToTarget.y));
        }

        return new Vector2(xDirection, yDirection);
    }
}
