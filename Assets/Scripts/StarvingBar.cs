using UnityEngine;
using UnityEngine.UI;

public class StarvingBar : MonoBehaviour
{
    [SerializeField] private Slider _slider;

    public void SetMaximumStarving(int maxWormAmount)
    {
        _slider.maxValue = maxWormAmount;
    }

    public void SetStarving(int currentEatenWormAmount)
    {
        _slider.value = currentEatenWormAmount;
    }
}
