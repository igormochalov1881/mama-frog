using System.Collections;
using UnityEngine;

public class Warm : MonoBehaviour
{
    private Rigidbody2D _rigidbody2D;
    [SerializeField] private Animator _animator;

    private Vector2 _playerPosition;
    private Vector2 _spawnPosition;
    private Vector2 _playerDirection;
    private Vector3 _rotationAngle;

    private float _speed;
    private float _rotationSpeed;
    private int _isRotatingRight;

    private bool _isSpoted;
    private bool _isRotating;
    private bool _isWondering;
    private bool _isWalking;

    private void Start()
    {
        _speed = Random.Range(0.35f, 0.55f);
        _rotationSpeed = Random.Range(70, 100);
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _spawnPosition = transform.position;
        _rotationAngle = new Vector3(0, 0, 90);
    }

    private void Update()
    {
        if (_isSpoted)
        {
            Run();
        }
        else
        {
            if (_isWondering == false)
            {
                StartCoroutine("Wondering");
            }

            if (_isRotatingRight == -1)
            {
                transform.Rotate(transform.forward, Time.deltaTime * _rotationSpeed);
            }
            else if (_isRotatingRight == 1)
            {
                transform.Rotate(-transform.forward, Time.deltaTime * _rotationSpeed);
            }
            else if (_isWalking)
            {
                _animator.SetBool("IsMoving", true);
                _rigidbody2D.AddForce(transform.right * _speed);
            }
        }
    }

    private IEnumerator Wondering()
    {
        int rotationTime = Random.Range(1, 3);
        int rotateWait = Random.Range(1, 3);
        int rotateDirection = Random.Range(1, 2);
        int walkWait = Random.Range(1, 3);
        int walkTime = Random.Range(1, 3);

        _isWondering = true;

        yield return new WaitForSeconds(walkWait);

        _isWalking = true;

        yield return new WaitForSeconds(walkTime);

        _animator.SetBool("IsMoving", false);
        _isWalking = false;

        yield return new WaitForSeconds(rotateWait);
        
        if (rotateDirection == 1)
        {
            _isRotatingRight = 1;
            yield return new WaitForSeconds(rotationTime);
            _isRotatingRight = 0;
        }
        else
        {
            _isRotatingRight = -1;
            yield return new WaitForSeconds(rotationTime);
            _isRotatingRight = 0;
        }

        _isWondering = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            _isSpoted = true;
            _isWondering = false;
        }
    }

    private void Run()
    {
        Vector2 oppositeToEnemyDirection = TargetDirection(transform.position, _playerPosition, _speed * 2) * -1;

        _animator.SetBool("IsMoving", true);
        _rigidbody2D.AddForce(oppositeToEnemyDirection);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            _isSpoted = false;
            _isWondering = true;
        }
    }
    
    private Vector2 TargetDirection(Vector2 objectPosition, Vector2 targetPosition, float speed)
    {
        float xDirection, yDirection;

        Vector2 distanceToTarget = new Vector2(targetPosition.x - objectPosition.x, targetPosition.y - objectPosition.y);

        if (Mathf.Abs(distanceToTarget.x) > Mathf.Abs(distanceToTarget.y))
        {
            xDirection = (distanceToTarget.x / Mathf.Abs(distanceToTarget.x)) * speed;
        }
        else
        {
            xDirection = Mathf.Abs(distanceToTarget.x) / Mathf.Abs(distanceToTarget.y) * ((distanceToTarget.x / Mathf.Abs(distanceToTarget.x)) * speed);
        }

        if (Mathf.Abs(distanceToTarget.y) > Mathf.Abs(distanceToTarget.x))
        {
            yDirection = (distanceToTarget.y / Mathf.Abs(distanceToTarget.y)) * speed;
        }
        else
        {
            yDirection = Mathf.Abs(distanceToTarget.y) / Mathf.Abs(distanceToTarget.x) * ((distanceToTarget.y / Mathf.Abs(distanceToTarget.y)) * speed);
        }

        return new Vector2(xDirection, yDirection);
    }
}
