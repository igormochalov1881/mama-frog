using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour
{
    [SerializeField] private GameObject _gameArea;
    [SerializeField] private GameObject _warmPrefab;

    public int _warmCount { get; set; } = 0;
    private int _warmLimit = 30;
    private int _warmPerFrame = 1;

    private float _spawnCircleRadius = 10f;

    private void Update()
    {
        MaintainPopulation();
    }

    private void MaintainPopulation()
    {
        if (_warmLimit > _warmCount)
        {
            for (int i = 0; i < _warmPerFrame; i++)
            {
                Vector3 position = GetRandomPosition();
                AddWarm(position);
            }
        }
    }

    private Vector3 GetRandomPosition()
    {
        Vector3 position = Random.insideUnitCircle;

        position *= _spawnCircleRadius;
        position += _gameArea.transform.position;
        position.z = 1;

        return position;
    }

    private void AddWarm(Vector3 position)
    {
        _warmCount++;

        GameObject newWarm = Instantiate(
            _warmPrefab, 
            position, 
            Quaternion.identity
        );
    }
}